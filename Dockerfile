FROM node:19-bullseye AS build-web

WORKDIR /usr/src/app
COPY ./web/package*.json ./
RUN npm install
COPY ./web/ ./
RUN npm run build

FROM node:19-bullseye AS build-api

WORKDIR /usr/src/app
COPY ./api/package*.json ./
RUN npm install
COPY ./api/ ./
RUN npm run build

FROM node:19-bullseye-slim AS runtime

# update runtime image, try to clean up container scans
RUN apt-get update && apt-get upgrade -y && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY --from=build-web /usr/src/app/dist ./web/dist
COPY --from=build-api /usr/src/app/dist ./api/dist
COPY ./api/package*.json ./api/dist/
RUN cd ./api/dist && npm install --production

WORKDIR /usr/src/app/api/dist
ENV PORT 5000
EXPOSE $PORT
ENV NODE_ENV "production"
CMD [ "npm", "run", "serve" ]
